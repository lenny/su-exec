PREFIX = $(DESTDIR)/usr

CFLAGS ?= -Wall -Werror -g
LDFLAGS ?=

PROG := su-exec
SRCS := $(PROG).c

all: $(PROG)

$(PROG): $(SRCS)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

$(PROG)-static: $(SRCS)
	$(CC) $(CFLAGS) -o $@ $^ -static $(LDFLAGS)

clean:
	rm -f $(PROG) $(PROG)-static

install: 
	install -D -m 755 su-exec $(PREFIX)/sbin/su-exec